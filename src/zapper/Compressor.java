package zapper;

import java.util.ArrayList;

/**
 * Created by Freddie on 14/03/2016.
 * Tool for compressing and decompressing Character Arrays.
 */
public class Compressor {
    /**
     * Compresses an Array of Characters.
     * Freddie
     *
     * @param uncompressedText Character Array to be compressed.
     * @return Compressed Character Array.
     */
    public static ArrayList<Character> compress(ArrayList<Character> uncompressedText)
            throws IndexOutOfBoundsException,
            InvalidFormat {

        ExceptionChecker.checkZapFormat(uncompressedText);


        ArrayList<Character> compressedText = new ArrayList<>();
        // adds the index and alphabet to the start of the return value
        for (int rename = 0; rename < (int) uncompressedText.get(0) + 1; rename++) {
            compressedText.add(uncompressedText.get(rename));
        }

        int concCharCount = 1; //number of concurrent characters
        compressedText.add(uncompressedText.get((int) uncompressedText.get(0) + 1));

        for (int streamIndex = (int) uncompressedText.get(0) + 2; streamIndex < uncompressedText.size(); streamIndex++) {
            if (!uncompressedText.get(streamIndex).equals(uncompressedText.get(streamIndex - 1))) {
                compressedText.add((char) concCharCount);
                compressedText.add(uncompressedText.get(streamIndex));
                concCharCount = 1;
            } else {
                concCharCount++;
            }
        }
        compressedText.add((char) concCharCount);
        return compressedText;
    }

    /**
     * Uncompresses a Character Array.
     * <p>
     * Freddie
     *
     * @param compressedText Character Array to be uncompressed.
     * @return Uncompressed Character Array.
     */
    public static ArrayList<Character> uncompress(ArrayList<Character> compressedText)
            throws IndexOutOfBoundsException,
            InvalidFormat {

        //checks that the input is valid
        ExceptionChecker.checkCzpFormat(compressedText);

        ArrayList<Character> uncompressedText = new ArrayList<>();

        // adds the index and alphabet to the start of the return value
        for (int index = 0; index < (int) compressedText.get(0) + 1; index++) {
            uncompressedText.add(compressedText.get(index));
        }

        for (int streamIndex = (int) compressedText.get(0) + 1; streamIndex < compressedText.size(); streamIndex += 2) {
            for (int index = 0; index < (int) compressedText.get(streamIndex + 1); index++) {
                uncompressedText.add(compressedText.get(streamIndex));
            }
        }

        return uncompressedText;
    }
}