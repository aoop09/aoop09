package zapper;

import java.io.*;
import java.util.ArrayList;

/**
 * @author stoddf1
 *         Reads and writes files with Character Arrays.
 */
public class FileHandler {
    /**
     * Returns the contents of a file as a Character Array.
     * <p>
     * Freddie
     *
     * @param fileName The location of file to be read.
     * @return All the contents of a file as a Character Array.
     * @throws java.io.IOException
     * @throws zapper.InvalidFileLocation
     */
    public static ArrayList<Character> readFile(String fileName)
            throws IOException,
            InvalidFileLocation {
        ArrayList<Character> charactersInFile = new ArrayList<>();

        BufferedReader openedFile = null;
        try {
            File fileToRead = new File(fileName);
            char[] charFileArray = new char[(int) fileToRead.length()];
            //Create Temporary Char[] to dump file contents

            openedFile = new BufferedReader(new FileReader(fileToRead));
            openedFile.read(charFileArray);
            //Dump file contents

            for (Character charInFile : charFileArray)
            //Convert Char[] to Character Array
            {
                charactersInFile.add(charInFile);
            }
        } catch (FileNotFoundException a) {
            InvalidFileLocation e = new InvalidFileLocation();
            e.setIncorrectFileLocation(fileName);
            throw e;
        } finally {
            if (openedFile != null) {
                openedFile.close();
            }
        }
        return charactersInFile;

    }

    /**
     * Writes a Character Array into a file. If the files doesn't exist it will create it.
     * <p>
     * Mitchell
     *
     * @param filename          The location of file to be written to.
     * @param charactersToWrite Character Array to be attached to file.
     * @throws java.io.IOException
     */
    public static void writeFile(String filename, ArrayList<Character> charactersToWrite)
            throws IOException {
        BufferedWriter fileWriter = null;
        try {
            File fileToBeWrittenTo = new File(filename);
            fileWriter = new BufferedWriter(new FileWriter(fileToBeWrittenTo));
            for (int characterIndex = 0; characterIndex != charactersToWrite.size(); characterIndex++)
            //Appends a character to the file for each in the array
            {
                fileWriter.write(charactersToWrite.get(characterIndex));
            }
        } finally {
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
    }
}
