/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zapper;

/**
 * @author warrm1
 *         Exception handler for if a file given is of the wrong format
 */
class InvalidFormat extends Exception {
    private String formatUsed = "";

    /**
     * Gets the format type that the user was trying to use.
     *
     * @return String of the intended format.
     */
    public String getFormatTried() {
        return formatUsed;
    }

    /**
     * Sets the intended format type that the user was trying to use.
     *
     * @param value String of the format type the user was trying.
     */
    public void setFormatTried(String value) {
        formatUsed = value;
    }

    /**
     * Gets a message about the exception.
     *
     * @return "Was not in the format of " + formatUsed + " ."
     */
    @Override
    public String getMessage() {
        return "Was not in the format of " + formatUsed + " .";
    }
}
