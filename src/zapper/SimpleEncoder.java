package zapper;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @author warrm1
 *         Takes a Character Array to encode.
 */
public class SimpleEncoder {
    /**
     * Takes a Character Array and returns an encoded Character Array.
     *
     * @param unencodedText Character Array to be encoded.
     * @return Encoded Character Array.
     */
    public static ArrayList<Character> encode(ArrayList<Character> unencodedText) {
        HashSet<Character> characterSet = new HashSet(unencodedText);
        //The Set will get rid of all double ups

        ArrayList<Character> encodedText = new ArrayList<>();
        //Sets index 0 to be the size of base alphabet

        encodedText.addAll(characterSet);

        for (Character uncodedCharacter : unencodedText) {
            encodedText.add((char) (encodedText.indexOf(uncodedCharacter)));
            //Finds the index of character in base Alphabet
            //Then sets that number as a Character
        }
        encodedText.add(0, (char) characterSet.size());
        return encodedText;
    }
}
