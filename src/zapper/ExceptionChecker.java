package zapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author warrm1
 *         Checks the exception handlers possible errors. Class contains methods that check for errors.
 */
public class ExceptionChecker {


    /**
     * Checks if a filename has the extension you want, or no extension at all.
     * <p>
     * Mitchell
     *
     * @param filename  Filename string to be checked.
     * @param extension The extension type to be checked against.
     * @throws InvalidFileExtension
     */
    public static void checkFileExtension(String filename, String extension)
            throws InvalidFileExtension {
        if (filename.contains(".")) {
            String[] filenameSplit = filename.split("\\.");
            //Split up the filename and the extension
            String filenameExtension = "." + filenameSplit[filenameSplit.length - 1];
            //Recreate the full extension from the last element

            if (!(filenameExtension.equals(extension))) {
                InvalidFileExtension e = new InvalidFileExtension();
                e.setExtensionType(extension);
                //Sets the extension we originally wanted
                e.setIncorrectUsedExtension(filenameExtension);
                //Sets the extension we got
                throw e;
            }
        }
    }

    /**
     * Checks if a file exists at the filename given.
     * <p>
     * Mitchell
     *
     * @param filename String location of file to be checked.
     * @throws InvalidFileLocation
     */
    public static void checkFileLocation(String filename)
            throws InvalidFileLocation {
        File newFile = new File(filename);
        if (!newFile.exists()) {
            //Checks if file exists
            InvalidFileLocation e = new InvalidFileLocation();
            e.setIncorrectFileLocation(filename);
            //Gives exception handler the file location
            throw e;
        }
    }

    /**
     * Takes a filename and correct extension wanted, and will validate that the filename has that extension, or give the extension to a filename without one. Then checks if file location exists.
     * <p>
     * Mitchell
     *
     * @param filename  File location String yet to be validated.
     * @param extension The extension required from the filename.
     * @return The validated filename String.
     * @throws InvalidFileExtension
     * @throws InvalidFileLocation
     * @throws IOException
     */
    public static String validateFilename(String filename, String extension)
            throws InvalidFileExtension,
            InvalidFileLocation,
            IOException
    //If any errors pop up they get thrown
    {
        ExceptionChecker.checkFileExtension(filename, extension);
        //Checks if the correct file extension is used
        if (!filename.endsWith(extension)) {
            filename += extension;
        }
        //Adds the correct extension on the end if no extension exists
        ExceptionChecker.checkFileLocation(filename);
        //Checks if the resulting filename exists
        return filename;
    }

    /**
     * Checks that the Character Array is in the zap format.
     * <p>
     * Mitchell
     *
     * @param encodedText A Character Array to be format checked.
     * @throws InvalidFormat
     */
    public static void checkZapFormat(ArrayList<Character> encodedText)
            throws InvalidFormat {
        int boundaries = (int) encodedText.get(0);
        //Holds this in a variable so that we don't need to get the value more than once
        boolean exceptionFound = false;

        if ((boundaries) - 1 > (encodedText.size() / 2))
        //If the first character suggests an alphabet larger than half the file, then it is not a zap format
        {
            exceptionFound = true;
        }

        for (int streamIndex = boundaries + 1; streamIndex < encodedText.size(); streamIndex++)
        //If any value is of an index value greater than the alphabet size, it is not a zap format
        {
            if ((int) encodedText.get(streamIndex) > boundaries) {
                exceptionFound = true;
            }
        }

        if (exceptionFound) {
            InvalidFormat e = new InvalidFormat();
            e.setFormatTried(".zap");
            throw e;
        }

    }

    /**
     * Checks that the Character Array is in the czp format
     * <p>
     * Freddie
     *
     * @param compressedText
     * @throws InvalidFormat
     */
    public static void checkCzpFormat(ArrayList<Character> compressedText)
            throws InvalidFormat {
        boolean exceptionFound = false;
        int boundaries = (int) compressedText.get(0);
        if ((boundaries) > ((compressedText.size() - 1) / 2)) {
            exceptionFound = true;
        }

        if ((compressedText.size() - (boundaries + 1)) % 2 != 0) {
            exceptionFound = true;
        }

        for (int i = boundaries + 1; i < compressedText.size(); i += 2) {
            if (compressedText.get(i) > boundaries) {
                exceptionFound = true;
            }
        }

        if (exceptionFound) {
            InvalidFormat e = new InvalidFormat();
            e.setFormatTried(".czp");
            throw e;
        }
    }
}
