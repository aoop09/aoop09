package zapper;

import java.io.*;

/**
 * @author warrm1
 */
public class Main {
    /***
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            if (args.length == 0) {

                BufferedReader consoleRead = new BufferedReader(new InputStreamReader(System.in));
                String[] command;
                do {
                    System.out.println(
                              "encode C:\\file\\location          *(.txt) will encode a file*\n"
                            + "decode C:\\file\\location          *(.zap) will decode a file*\n"
                            + "compress C:\\file\\location        *(.txt) will compress a file*\n"
                            + "uncompress C:\\file\\location      *(.czp) will decompress a file*\n"
                            + "showFile C:\\file\\location        *(.txt) will print contents of file*\n"
                            + "showDecodedFile C:\\file\\location *(.zap) will decode a file and print on screen*\n"
                            + "end                              *will end program*\n");
                    command = consoleRead.readLine().split(" ", 2);
                    try {
                        doCommand(command);
                    } catch (InvalidFileExtension e) {
                        System.out.println("\n! Wrong extension. " + e.getMessage() + " !");
                    } catch (InvalidFileLocation e) {
                        System.out.println("\n! File does not exist. " + e.getMessage() + " !");
                    } catch (InvalidFormat e) {
                        System.out.println("\n! File was in an invalid format. " + e.getMessage() + " !");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("\n! The file you chose is not in a valid format. Please try another file. !");
                    } finally {
                        if (!command[0].equals("end")) {
                            System.out.println("\nPress any key to continue");
                            consoleRead.readLine();
                            System.out.println("\n\n");
                        }
                    }
                }
                while (!command[0].equals("end"));
            } else {
                if (args[0].toLowerCase().equals("help")){
                    System.out.print(
                              "Usage: java zapper.main [option] [filepath]\n"
                            + "where filepath is the location of the file you want to modify\n"
                            + "where options include:\n"
                            + "encode            use to encode a .txt file to .zap file\n"
                            + "decode            use to decode a .zap file to .txt file\n"
                            + "compress          use to compress a .txt file to a .czp file\n"
                            + "uncompress        use to uncompress a .czp file to a .txt file\n"
                            + "showFile          use to print a .txt file to the terminal\n"
                            + "showDecodedFile   use to print a .zap file to the terminal after decoding it to plain text\n");
                } else {
                    try {
                        doCommand(args);
                    } catch (InvalidFileExtension e) {
                        System.out.println("\n! Wrong extension. " + e.getMessage() + " !");
                    } catch (InvalidFileLocation e) {
                        System.out.println("\n! File does not exist. " + e.getMessage() + " !");
                    } catch (InvalidFormat e) {
                        System.out.println("\n! File was in an invalid format. " + e.getMessage() + " !");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("\n! The file you chose is not in a valid format. Please try another file. !");
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("System IO Error\n");
        }
    }

    static void doCommand(String[] args)
            throws IOException,
            InvalidFileLocation,
            InvalidFormat {
        switch (args[0].toLowerCase()) {
            case "encode":
                FileConverter.encode(args[1]);
                System.out.println("\nSuccessfully Encoded");
                break;
            case "decode":
                FileConverter.decode(args[1]);
                System.out.println("\nSuccessfully Decoded");
                break;
            case "compress":
                FileConverter.compress(args[1]);
                System.out.println("\nSuccessfully Compressed");
                break;
            case "uncompress":
                FileConverter.uncompress(args[1]);
                System.out.println("\nSuccessfully Uncompressed");
                break;
            case "showfile":
                FileConverter.showFile(args[1]);
                System.out.println("\n----------End Of File----------");
                break;
            case "showdecodedfile":
                FileConverter.decodeAndShowFile(args[1]);
                System.out.println("\n----------End Of File----------");
                break;
            case "end":
                System.out.println("\nShutting down. Thank you for using zapper© AOOP09");
                break;
            default:
                System.out.println("\nInput needs to be 'command fileLocation'");
        }
    }
}
