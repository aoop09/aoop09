package zapper;

import java.io.*;

/**
 * @author warrm1
 *         Exception handler for if a user can try to use the wrong file extention.
 */
class InvalidFileExtension extends IOException {
    private String extensionType = "";
    private String incorrectUsedExtension = "";

    /**
     * Gets the extension the user should have used.
     *
     * @return A String of the extension the user should have used.
     */
    public String getExtensionType() {
        return extensionType;
    }

    /**
     * Sets what extension the user should have used.
     *
     * @param value String to be set
     */
    public void setExtensionType(String value) {
        extensionType = value;
    }

    /**
     * Gets the extension the user incorrectly used.
     *
     * @return A String of the extension the user incorrectly used.
     */
    public String getIncorrectUsedExtension() {
        return incorrectUsedExtension;
    }

    /**
     * Sets what extension the user incorrectly used.
     *
     * @param value String to be set
     */
    public void setIncorrectUsedExtension(String value) {
        incorrectUsedExtension = value;
    }

    /**
     * A short message detailing what extension was used and what extension should have been used.
     *
     * @return "Extension '"+incorrectUsedExtension+"' used. Required extension is '"+extensionType+"'"
     */
    @Override
    public String getMessage() {
        return "Extension '" + incorrectUsedExtension + "' used. Required extension is '" + extensionType + "'";
    }
}
