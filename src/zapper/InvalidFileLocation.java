package zapper;

/**
 * @author warrm1
 *         Exception handler for if a file location can not be found.
 */
class InvalidFileLocation extends Exception {
    private String incorrectFileLocation = "";

    /**
     * Gets the value of the user inputted filename.
     *
     * @return The user inputted filename.
     */
    public String getIncorrectFileLocation() {
        return incorrectFileLocation;
    }

    /**
     * Sets the value of user inputted filename.
     *
     * @param value String to be set.
     */
    public void setIncorrectFileLocation(String value) {
        incorrectFileLocation = value;
    }

    /**
     * A short message detailing the failed file location.
     *
     * @return "Cannot find file location '"+incorrectFileLocation+"'"
     */
    @Override
    public String getMessage() {
        return "Cannot find file location '" + incorrectFileLocation + "'";
    }
}
