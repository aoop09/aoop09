package zapper;

import java.io.*;

/**
 * @author warrm1
 *         The tool used to make use of the other classes.
 *         The class available to the main method.
 */
public class FileConverter {

    /**
     * Reads from a text file and encodes the contents into a .zap file. Will create a .zap file from encoded array.
     *
     * @param filename Location of file to be encoded. Will append ".txt" to files without the suffix.
     * @throws zapper.InvalidFileExtension
     * @throws zapper.InvalidFileLocation
     */
    public static void encode(String filename)
            throws InvalidFileExtension,
            InvalidFileLocation,
            IOException {
        filename = ExceptionChecker.validateFilename(filename, ".txt");
        //Will make sure that the filename is both valid and exists

        String zapFilename = filename.substring(0, filename.lastIndexOf('.') + 1) + "zap";
        //Creates the filename of the encoded file
        FileHandler.writeFile(zapFilename,
                SimpleEncoder.encode(
                        FileHandler.readFile(filename)));
    }

    /**
     * Reads from a .zap file and decodes the contents back into a text file. Will create a .txt file from decoded array
     *
     * @param filename Location of file to be decoded. Will append ".zap" to files without the suffix.
     * @throws zapper.InvalidFileExtension
     * @throws zapper.InvalidFileLocation
     * @throws zapper.InvalidFormat
     */
    public static void decode(String filename)
            throws InvalidFileExtension,
            InvalidFileLocation,
            InvalidFormat,
            IOException {
        filename = ExceptionChecker.validateFilename(filename, ".zap");
        //Will make sure that the filename is both valid and exists

        String txtFilename = filename.substring(0, filename.lastIndexOf('.') + 1) + "txt";
        //Creates the filename of the encoded file
        FileHandler.writeFile(txtFilename,
                SimpleDecoder.decode(
                        FileHandler.readFile(filename)));
    }

    /**
     * Reads from a .txt file to compress its contents into a .czp file. Will create a .czp file.
     *
     * @param filename Location of .txt file to be compressed.
     * @throws zapper.InvalidFileExtension
     * @throws zapper.InvalidFileLocation
     */
    public static void compress(String filename)
            throws InvalidFileExtension,
            InvalidFileLocation,
            IOException,
            IndexOutOfBoundsException,
            InvalidFormat {
        filename = ExceptionChecker.validateFilename(filename, ".txt");
        //Will make sure that the filename is both valid and exists

        String czpFilename = filename.substring(0, filename.lastIndexOf('.') + 1) + "czp";
        //Creates the filename of the encoded file
        FileHandler.writeFile(czpFilename,
                Compressor.compress(
                        SimpleEncoder.encode(
                                FileHandler.readFile(filename))));
    }

    /**
     * Reads from a .czp file to uncompress its contents into a .txt file. Will create a .txt file.
     *
     * @param filename Location of .czp to be uncompressed.
     * @throws zapper.InvalidFileExtension
     * @throws zapper.InvalidFileLocation
     * @throws zapper.InvalidFormat
     */
    public static void uncompress(String filename)
            throws InvalidFileExtension,
            InvalidFileLocation,
            IOException,
            InvalidFormat,
            IndexOutOfBoundsException {
        filename = ExceptionChecker.validateFilename(filename, ".czp");
        //Will make sure that the filename is both valid and exists

        String txtFilename = filename.substring(0, filename.lastIndexOf('.') + 1) + "txt";
        //Creates the filename of the encoded file
        FileHandler.writeFile(txtFilename,
                SimpleDecoder.decode(
                        Compressor.uncompress(
                                FileHandler.readFile(filename))));
    }

    /**
     * Prints out the contents of a text file to the console.
     *
     * @param filename Location of the file to be shown. Will append ".txt" to files without the suffix.
     * @throws zapper.InvalidFileExtension
     * @throws zapper.InvalidFileLocation
     */
    public static void showFile(String filename)
            throws InvalidFileExtension,
            InvalidFileLocation,
            IOException {
        filename = ExceptionChecker.validateFilename(filename, ".txt");
        //Will make sure that the filename is both valid and exists

        for (Character nextCharacter : FileHandler.readFile(filename)) {
            System.out.print(nextCharacter);
        }
        System.out.println();
    }

    /**
     * Prints out the contents of a zap file onto the console. It decodes before printing.
     *
     * @param filename Location of the file to be decoded and shown. Will append ".zap" to files without the suffix.
     * @throws zapper.InvalidFileExtension
     * @throws zapper.InvalidFileLocation
     * @throws zapper.InvalidFormat
     */
    public static void decodeAndShowFile(String filename)
            throws InvalidFileExtension,
            InvalidFileLocation,
            InvalidFormat,
            IOException {
        filename = ExceptionChecker.validateFilename(filename, ".zap");
        //Will make sure that the filename is both valid and exists

        for (Character nextCharacter : SimpleDecoder.decode(FileHandler.readFile(filename))) {
            System.out.print(nextCharacter);
        }
        System.out.println();
    }

}
