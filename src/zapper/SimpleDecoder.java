package zapper;

import java.util.ArrayList;

/**
 * @author warrm1
 *         Used to decode a list of encoded Characters.
 */
public class SimpleDecoder {
    /**
     * Takes an encoded Array of Characters and returns a decoded Character Array.
     *
     * @param encodedText Encoded Character Array.
     * @return Decoded Character Array.
     * @throws zapper.InvalidFormat
     */
    public static ArrayList<Character> decode(ArrayList<Character> encodedText)
            throws InvalidFormat {
        ArrayList<Character> decodedText = new ArrayList<>();
        ExceptionChecker.checkZapFormat(encodedText);
        for (int streamIndex = (int) encodedText.get(0) + 1; streamIndex < encodedText.size(); streamIndex++)
        //index = number indicating size of base alphabet
        //index thus starts from the first index after alphabet
        {
            decodedText.add(encodedText.get((int) encodedText.get(streamIndex) + 1));
            //get integer value of streamIndex
            //add corresponding Character at its indicated index
        }
        return decodedText;
    }
}
