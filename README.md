# Zapper #

Zapper is a java program that converts text files between .txt, .zap and .czp formats

* **.zap** is a intermediate format between .txt and .czp which is actually bigger than the original text file
* **.czp** is a compressed format of text that is only effective on files with lots of repeated characters

### Using Zapper ###

Java version: 1.8 (was built and tested with 1.8 and hasn't been tested with any other versions)

You can run zapper from source by running a the following commands from the AOOP09 directory

#### Windows ####

```
    mkdir bin
    cd src\zapper
    javac -d ..\..\bin *.java
    cd ..\..
    java -classpath bin zapper.Main [params]
```
this will work on windows if the JDK and JRE are added to your path

#### Linux ####

```
    mkdir bin
    cd src/zapper
    javac -d ../../bin *.java
    cd ../..
    java -classpath bin zapper.Main [params]
```

Zapper can be run with or without parameters.

When running with parameters zapper will complete the task and then exit.

When running without parameters zapper will ask you what you want to do until you type end.

#### Procedures Zapper Can Perform ####

 * **encode [file path]**: encodes a .txt file at given path to .zap format
 * **decode [file path]**: decodes a .zap file at given path to .txt format
 * **compress [file path]**: compresses a .txt file at given path to .czp format
 * **uncompress [file path]**: uncompresses a .czp file at given location to .txt format
 * **showFile [file path]**: prints a .txt file from given path to terminal
 * **showDecodedFile [file path]**: decodes and prints a .zap file at path given to the terminal


### Setup ###

#### Windows ####

Install [git](https://git-scm.com/download/win)

Install [JDK](http://download.oracle.com/otn-pub/java/jdk/8u77-b03/jdk-8u77-windows-i586.exe) and [JRE](http://download.oracle.com/otn-pub/java/jdk/8u77-b03/jre-8u77-windows-i586.exe)

Run the following command in a location where you want to clone to.

`git clone https://bitbucket.org/aoop09/aoop09.git`

#### Linux (Ubuntu) ####

Run the following commands in the terminal from the path you want to install zapper in
```
    sudo apt-get update
    sudo apt-get install git default-jdk
    git clone https://bitbucket.org/aoop09/aoop09.git
```